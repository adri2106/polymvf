// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:
    {
      apiKey: "AIzaSyCtLOttYk6-0-gaep4kwX8pJhYvG6Fdggk",
      authDomain: "polym-c86db.firebaseapp.com",
      databaseURL: "https://polym-c86db.firebaseio.com",
      projectId: "polym-c86db",
      storageBucket: "polym-c86db.appspot.com",
      messagingSenderId: "486748180678"
    }
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
