import { Component, OnInit } from "@angular/core";
import { PolymfbService } from "src/app/services/polymfb.service";
import { FormGroup, FormControl } from "@angular/forms";

@Component({
  selector: "app-plantilla",
  templateUrl: "./plantilla.component.html",
  styles: []
})
export class PlantillaComponent implements OnInit {
  public jugadores: any = [];
  public goleadores: any = [];
  public amonestados:any = [];
  

  public posicion: string;
  public filtro: FormGroup;

  constructor(private pS: PolymfbService) {
    this.filtro = new FormGroup({
      posicion: new FormControl("TODOS")
    });
  };
  cargarGoleadores(){
    this.pS.verTodos().subscribe(res=>{
     this.goleadores = Object.values(res);
         this.goleadores.sort(
       function(a,b){
         return (b.goles-a.goles)},
         console.log(this.goleadores)
    )
    const pichichi = this.goleadores.filter((item, i)=>{
      // console.log(item.goles);
      
      return i < 4 && item.goles> 0;
      
    }
       );
    this.goleadores = pichichi;
    console.log(pichichi)
  })}

cargarAmonestados(){
this.pS.verTodos().subscribe(res=>{
this.amonestados= Object.values(res);
this.amonestados.sort(
  function(a,b){
    return (b.ta -a.ta)},
)
    const sucio= this.amonestados.filter((elem, i)=> i<3);
    this.amonestados = sucio;
  })
}



 
  filtrarPosicion() {
    this.jugadores = [];
    this.posicion = this.filtro.value;
    this.pS.verTodos().subscribe(res => {
      this.jugadores = [];
      const keys = Object.keys(res);

      for (let i = 0; i < keys.length; i++) {
        const jug = res[keys[i]];
        console.log(jug["posicion"]);
        const pos = Object.values(this.posicion);
        console.log(pos);
        console.log(pos[0]);
        if(pos[0]=== 'TODOS'){
          this.jugadores.push(jug);
        }else if (jug["posicion"] === pos[0]) {
          this.jugadores.push(jug);
        }
      }
    });
  }

  ngOnInit() {
    this.filtrarPosicion();
    this.cargarGoleadores();
    this.cargarAmonestados();
  }
}
