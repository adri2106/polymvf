import { Component, OnInit } from "@angular/core";
import { Validators, FormGroup, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { PolymfbService } from "src/app/services/polymfb.service";


@Component({
  selector: "app-edicion",
  templateUrl: "./edicion.component.html",
  styles: []
})
export class EdicionComponent implements OnInit {
  private jugador: any = [];

  public pForma: FormGroup;
  constructor(
    public pS: PolymfbService,
    private router: Router,
    private actRoute: ActivatedRoute
  ) {
    this.pForma = new FormGroup({
      nombre: new FormControl("", Validators.required),
      dorsal: new FormControl("", Validators.required),
      posicion: new FormControl("", Validators.required),
      comentarios: new FormControl(""),
      goles: new FormControl(0),
      ta: new FormControl(0),
      imagen: new FormControl(""),
        });
  }

ingresar(proveedor){
  console.log(proveedor);
  this.pS.login(proveedor)
}


  cancelarEnvio(ev) {
    ev.preventDefault();
  }
  guardar() {
    this.jugador = this.pForma.value;
   const id = this.actRoute.snapshot.params.id;
   
      if (id === "new") {
        this.nuevo();
      } else {
        this.actualizar(id);
      }
    };
  
  nuevo() {
    this.pS.crear(this.jugador).subscribe((resultado: any) => {
      console.log(resultado);
      this.router.navigate(["/estadisticas"]);
    });
  }
  actualizar(id: string) {
    this.pS.actualizar(this.jugador, id).subscribe(resultado => {
      console.log(resultado);
      this.router.navigate(["/estadisticas"]);
    });
  }

  ngOnInit() {
    if (this.actRoute.snapshot.params.id !== "new") {
      const id = this.actRoute.snapshot.params.id;
      this.pS.verUno(id).subscribe(res => {
        console.log(res);
        if (res === null) {
          this.router.navigate(["/edicion", "new"]);
        } else {
          this.pForma.setValue(res);
        }
      });
    }
  }
}
