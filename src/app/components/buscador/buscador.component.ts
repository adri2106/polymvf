import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { PolymfbService } from "src/app/services/polymfb.service";

@Component({
  selector: "app-buscador",
  templateUrl: "./buscador.component.html",
  styles: []
})
export class BuscadorComponent implements OnInit {
  public jugador: any = [];
  public termino: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private pS: PolymfbService
  ) {}

  ngOnInit() {
    this.jugador = [];
    this.activatedRoute.params.subscribe(params => {
      this.termino = params.term;
      this.pS.verTodos().subscribe(res => {
        this.jugador = [];
        const keys = Object.keys(res);
        for (let i = 0; i < keys.length; i++) {
          const jug = res[keys[i]];
          
          if (
            jug["nombre"].toLowerCase().indexOf(params.term.toLowerCase()) >= 0
          ) {
            jug.id = keys[i];
            this.jugador.push(jug);
          }
        }
      });
    });
  }
}
