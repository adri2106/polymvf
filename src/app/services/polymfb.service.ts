import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ficha} from './../ficha';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class PolymfbService {

  private url = 'https://polym-c86db.firebaseio.com/jugadores.json';
  private urlJ = 'https://polym-c86db.firebaseio.com/jugadores';
  public usuario:any ={};
  constructor(private http: HttpClient,
    public afAuth: AngularFireAuth) { 
this.afAuth.authState.subscribe(user =>{
  console.log('Estado del usuario:', user)
  if(!user){
    return;
  }
  this.usuario.nombre = user.displayName;
  this.usuario.uid = user.uid;
})


    }

    login(proveedor:string) {
      if(proveedor ==='Google'){
      this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
    }else{
      this.afAuth.auth.signInWithPopup(new auth.TwitterAuthProvider());
    }}
    logout() {
      this.usuario ={}
      this.afAuth.auth.signOut();
    }

crear(j:ficha){
  return this.http.post(this.url,j)
}
verTodos(){
  
  return this.http.get(this.url)
}
actualizar(j:ficha, id:string){
 const urlF = `${this.urlJ}/${id}.json`
 return this.http.put(urlF, j)
}
verUno(id: string) {
  const url = `${this.urlJ}/${id}.json`;
  return this.http.get(url);
}
eliminar(id:string){
const urlF=`${this.urlJ}/${id}.json`
return this.http.delete(urlF)
}
}
