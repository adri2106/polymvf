import { Component, OnInit } from '@angular/core';
import{ Router} from '@angular/router'
import { PolymfbService } from 'src/app/services/polymfb.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: []
})
export class NavbarComponent implements OnInit {

  constructor(private router: Router,
    public pS:PolymfbService) { }

buscar(term:string){
  this.router.navigate(['/buscar',term])
}
salir(){
  this.pS.logout()
}
  ngOnInit() {
  }

}
